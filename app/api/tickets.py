from flask import jsonify, request, url_for, g, abort
from app import db
from app.models import Ticket, User
from app.api import bp
from app.api.auth import token_auth
from app.api.errors import bad_request


@bp.route('/tickets/<int:id>', methods=['GET'])
@token_auth.login_required
def get_ticket(id):
    return jsonify(Ticket.get_or_404(id).to_dict())



@bp.route('tickets', methods=['GET'])
@token_auth.login_required
def get_tickets():
    user = g.current_user
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    print(Ticket.customq())
    ticket_custom = Ticket.customq().filter_by(user_id=user.id)
    data = Ticket.to_collection_dict(ticket_custom, page, per_page,
                                   'api.get_tickets')
    return jsonify(data)


@bp.route('/tickets', methods=['POST'])
@token_auth.login_required
def create_ticket():
    data = request.get_json() or {}
    if 'body' not in data or 'platform' not in data:
        return bad_request('must include body, platform fields')
    if Ticket.query.filter_by(body=data['body']).first():
        return bad_request('please use a different body, duplication body')
    user_id = g.current_user.id
    data['user_id']=user_id
    ticket = Ticket()
    ticket.from_dict(data)
    db.session.add(ticket)
    db.session.commit()
    response = jsonify(ticket.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_ticket', id=ticket.id)
    return response


@bp.route('/tickets/<int:id>', methods=['PUT'])
@token_auth.login_required
def update_ticket(id):
    ticket = Ticket.query.get_or_404(id)
    data = request.get_json() or {}
    if 'body' not in data or 'platform' not in data:
        return bad_request('must include body, platform fields')
    if g.current_user.id != ticket.user_id:
        abort(403)
    ticket.from_dict(data)
    db.session.commit()
    return jsonify(ticket.to_dict())

@bp.route('/tickets/<int:id>', methods=['DELETE'])
@token_auth.login_required
def delete_ticket(id):
    ticket = Ticket.query.get_or_404(id)
    if g.current_user.id != ticket.user_id:
        abort(403)
    if not ticket.deleted:
        data = {}
        ticket.from_dict(data, deleted=True)
        db.session.commit()
    return jsonify(ticket.to_dict())
