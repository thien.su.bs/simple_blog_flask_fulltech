from flask import jsonify, request, url_for, g, abort
from app import db
from app.models import Ticket, User, Message
from app.api import bp
from app.api.auth import token_auth
from app.api.errors import bad_request


@bp.route('/message/<int:id>', methods=['GET'])
@token_auth.login_required
def get_message(id):
    user = g.current_user
    message = Message.query.filter_by(id=id, user_id=user.id)
    jsonify(message.to_dict())

@bp.route('/messages/<int:ticket_id>', methods=['GET'])
@token_auth.login_required
def get_messages(ticket_id):
    user = g.current_user
    ticket=Ticket.query.get_or_404(ticket_id)
    if ticket is None or ticket.deleted:
        return jsonify(ticket.to_dict())
    messages = Message.query.filter_by(user_id=user.id, ticket_id=ticket.id)
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = User.to_collection_dict(messages, page, per_page,
                                   'api.get_messages', ticket_id=ticket_id)
    return jsonify(data)


@bp.route('/messages/<int:ticket_id>', methods=['POST'])
@token_auth.login_required
def create_message(ticket_id):
    data = request.get_json() or {}
    if 'body' not in data:
        return bad_request('must include body fields')
    user_id = g.current_user.id
    data['user_id']=user_id
    message = Message()
    data['ticket_id']=ticket_id
    message.from_dict(data)
    db.session.add(message)
    db.session.commit()
    response = jsonify(message.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_message', id=message.id)
    return response


@bp.route('/messages/<int:id>', methods=['PUT'])
@token_auth.login_required
def update_message(id):
    ticket = Message.query.get_or_404(id)
    data = request.get_json() or {}
    if 'body' not in data:
        return bad_request('must include body fields')
    if g.current_user.id != ticket.user_id:
        abort(403)
    ticket.from_dict(data)
    db.session.commit()
    return jsonify(ticket.to_dict())

@bp.route('/tickets/<int:id>', methods=['DELETE'])
@token_auth.login_required
def delete_message(id):
    ticket = Ticket.query.get_or_404(id)
    if g.current_user.id != ticket.user_id:
        abort(403)
    if not ticket.deleted:
        data = {}
        ticket.from_dict(data, deleted=True)
        db.session.commit()
    return jsonify(ticket.to_dict())
