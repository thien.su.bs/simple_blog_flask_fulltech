FROM python:3.8-alpine3.11

RUN adduser -D deploy

WORKDIR /home/deploy

COPY requirements.txt requirements.txt
RUN python -m venv venv
RUN pip install --upgrade pip
RUN venv/bin/pip install -r requirements.txt
RUN venv/bin/pip install gunicorn pymysql

COPY app app
COPY migrations migrations
COPY simple_blog.py config.py start.sh ./
RUN chmod a+x start.sh

ENV FLASK_APP simple_blog.py

RUN chown -R deploy:deploy ./
USER deploy

EXPOSE 8080
ENTRYPOINT ["./start.sh"]
