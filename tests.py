#!/usr/bin/env python
from datetime import datetime, timedelta
import unittest
from app import create_app, db
from app.models import User, Ticket, Message
from config import Config


class TestConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    ELASTICSEARCH_URL = None


class UserModelCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app(TestConfig)
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_password_hashing(self):
        u = User(username='susan')
        u.set_password('cat')
        self.assertFalse(u.check_password('dog'))
        self.assertTrue(u.check_password('cat'))

    def test_avatar(self):
        u = User(username='john', email='john@example.com')
        self.assertEqual(u.avatar(128), ('https://www.gravatar.com/avatar/'
                                         'd4c74594d841139328695756648b6bd6'
                                         '?d=identicon&s=128'))

    def test_init_data_consit(self):
        u1 = User(username='john', email='john@example.com')
        u2 = User(username='susan', email='susan@example.com')
        db.session.add(u1)
        db.session.add(u2)
        db.session.commit()
        self.assertEqual(u1.tickets.all(), [])
        self.assertEqual(u1.messages.all(), [])
        self.assertEqual(u2.tickets.all(), [])
        self.assertEqual(u2.messages.all(), [])

    def test_create_tickets_and_reply_message(self):
        # create four users
        u1 = User(username='john', email='john@example.com')
        u2 = User(username='susan', email='susan@example.com')
        u3 = User(username='mary', email='mary@example.com')
        u4 = User(username='david', email='david@example.com')
        db.session.add_all([u1, u2, u3, u4])

        # create four tikets
        now = datetime.utcnow()
        t1 = Ticket(body="What is Lorem Ipsum?", platform="ios", author=u1,
                  timestamp=now + timedelta(seconds=1))
        t2 = Ticket(body="Why do we use it?", platform="ios", author=u2,
                  timestamp=now + timedelta(seconds=4))
        t3 = Ticket(body="Where can I get some?",platform="android", author=u3,
                  timestamp=now + timedelta(seconds=3))
        t4 = Ticket(body="Where does it come from?", platform="android", author=u4,
                  timestamp=now + timedelta(seconds=2))
        db.session.add_all([t1, t2, t3, t4])
        db.session.commit()

        # setup the messages
        m1=Message(body= "dummy text ever since the 1500s", ticket_id=t1.id, user_id=u1.id)
        m2=Message(body= "like Aldus PageMaker including versions of Lorem Ipsum.", ticket_id=t1.id, user_id=u1.id)
        m3=Message(body= "It is a long established fact that a reader will be distracted ", ticket_id=t2.id, user_id=u1.id)
        m4=Message(body= " Many desktop publishing packages and web page editors", ticket_id=t2.id, user_id=u2.id)
        m6=Message(body= "ontrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece o", ticket_id=t3.id, user_id=u3.id)
        m7=Message(body= "ections 1.10.32 and 1.10.33 of ", ticket_id=t3.id, user_id=u3.id)
        m5=Message(body= "If you are going to use a passage of Lorem Ipsum", ticket_id=t4.id, user_id=u4.id)
        
        db.session.add_all([m1,m2,m3,m4,m5,m6,m7])
        db.session.commit()

        # check the messages of each user
        f1 = u1.messages
        f2 = u2.messages
        f3 = u3.messages
        f4 = u4.messages.all()
        self.assertEqual(f1.count(), 3)
        self.assertEqual(f2.count(), 1)
        self.assertEqual(f3.count(), 2)
        self.assertEqual(f4, [m5])

        # check the ticket of each user
        tf1 = u1.tickets.all()
        tf2 = u2.tickets.all()
        tf3 = u3.tickets.all()
        tf4 = u4.tickets.all()
        self.assertEqual(tf1, [t1])
        self.assertEqual(tf2, [t2])
        self.assertEqual(tf3, [t3])
        self.assertEqual(tf4, [t4])

        # check the messages of each ticket
        tm1 = t1.messages
        tm2 = t2.messages
        tm3 = t3.messages
        tm4 = t4.messages
        self.assertEqual(tm1.count(), 2)
        self.assertEqual(tm2.all(), [m3,m4])
        self.assertEqual(tm3.count(), 2)
        self.assertEqual(tm4.count(), 1)

if __name__ == '__main__':
    unittest.main(verbosity=2)
