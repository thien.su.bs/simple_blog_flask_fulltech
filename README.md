# simple_blog_flask_fulltech

* How to run the test suite (unit test)

```
python tests.py

```
* Run Python Simple application via docker-compose: (MariaDB + redis + Flask)

  1. ```docker-compose pull ```

  2. ```docker-compose build```

  3. ```docker-compose up```


* Some NOTE: 
  
  The Port HTTP application listening: ```8080```
  API endpoint: /api

  
